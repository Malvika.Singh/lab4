package no.uib.inf101.gridview;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;
import java.awt.geom.Rectangle2D;
 

public class CellPositionToPixelConverter {

  //koordinater i rutenettet til et rektangel
  //med posisjon og størrelse beskrevet som piksler

  Rectangle2D box;
  GridDimension gd;
  double margin;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  public Rectangle2D getBoundsForCell(CellPosition cp){
    double cellHeight = (box.getHeight() - ((gd.rows() + 1)*margin))/gd.rows();  
    double cellWidth = (box.getWidth() - ((gd.cols() + 1)*margin))/gd.cols();
    double cellY = box.getY() + margin + (cellHeight + margin)*(cp.row());
    double cellX = box.getX() +  margin + (cellWidth + margin)*(cp.col());

    Rectangle2D box = new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
    return box;
  }
}
