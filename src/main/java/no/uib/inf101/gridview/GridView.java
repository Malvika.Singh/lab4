package no.uib.inf101.gridview;

 
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;
import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;
import java.awt.geom.Rectangle2D;
import java.util.List;
import java.awt.Color;
import no.uib.inf101.colorgrid.CellPosition;

 
public class GridView extends JPanel{

 
  //Field variables
  IColorGrid grid;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

 
  //Constructor
  public GridView(IColorGrid grid) {
    this.setPreferredSize(new Dimension(400,300));
    this.grid = grid;
  }


  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2);
  }




  private static void drawCells(Graphics2D g, CellColorCollection c, CellPositionToPixelConverter cp) {

  List<CellColor> cellList = c.getCells();
  for (CellColor cell : cellList) {
    Color color = cell.color();
    CellPosition pos = cell.cellPosition();
    if (color==null) {
      color = Color.DARK_GRAY;
    }
    Rectangle2D g2 = cp.getBoundsForCell(pos);
    g.setColor(color);
    g.fill(g2);
  }
}

 

  public void drawGrid(Graphics2D g){
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;
    Rectangle2D g2 = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, width, height);

    g.setColor(MARGINCOLOR);
    g.fill(g2);

    CellPositionToPixelConverter cp = new CellPositionToPixelConverter(g2, grid, OUTERMARGIN);
    drawCells(g, grid, cp);
  }
}


