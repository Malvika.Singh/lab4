package no.uib.inf101.colorgrid;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;


public class ColorGrid implements IColorGrid {

  // Field variables
  private int rows;
  private int cols;
  private List<ArrayList<Color>> grid;
   

  // konstruktør med to parameter
  public ColorGrid (int rows, int cols) {
    this.rows = rows;
    this.cols = cols;
    grid = new ArrayList<ArrayList<Color>> ();


    for (int i =0; i < rows; i++) {
      ArrayList<Color> gridRow = new ArrayList<Color>();
      for (int j=0; j < cols; j++) {
        gridRow.add(null);
      }
      grid.add(gridRow);
    }
  }

  @Override
  public int rows() {
    return this.rows; 
  }

  @Override
  public int cols() {
    return this.cols;  
  }


  @Override
  public Color get(CellPosition pos) {
    int i = pos.row();
    int j = pos.col();
    return  grid.get(i).get(j); }

  @Override
  public void set(CellPosition pos, Color color) {
    int i = pos.row();
    int j = pos.col();
    grid.get(i).set(j, color);
  }

  @Override
  public List<CellColor> getCells() {
    List<CellColor> cellList = new ArrayList<CellColor>();

    for (int i =0; i < grid.size(); i++) {
      for (int j=0; j < grid.get(i).size(); j++) {
        Color color = grid.get(i).get(j);
        CellPosition pos = new CellPosition(i, j);
        CellColor cell = new CellColor(pos, color);
        cellList.add(cell);
      }
    }
    return cellList;
  }
}
